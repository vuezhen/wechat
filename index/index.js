//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    qrcode: 'http://res.youth.cn/article_201810_24_244_5bcfa0030fb10.jpg',
    motto: 'Hello World',
    array: [{
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }, {
      phone: '186****1685',
      date: '2018-10-22 09:41',
    }],
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    
  },
  onPageScroll(e) {
    let _this = this;
    wx.getSystemInfo({
      success(res) {
        let a = res.windowHeight;
        wx.createSelectorQuery().select('.invitation').boundingClientRect(function (rect) {
          let b = rect.height;
          if (b - a - e.scrollTop < 100) {
            wx.showLoading({
              title: '加载中',
              mask: true
            })
            let list = [];
            for (let i = 0; i < 15; i++) {
              list.push({ phone: '186****1685', date: '2018-10-22 09:41', })
            }
            let arr = _this.data.array;
            arr = arr.concat(list)
            setTimeout(() => {
              _this.setData({
                array: arr
              })
              wx.hideLoading()
            }, 200)
          }
        }).exec()
      }
    })
  },
  // 上拉加载，拉到底部触发
  onReachBottom() {

  },
})
